package com.youmusic.magictiles;

import com.google.firebase.messaging.MessageForwardingService;
import com.unity3d.player.*;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import java.io.File;
import java.io.*;
import java.io.Console;
import java.io.FileDescriptor;
import net.nuna.PermissionManager;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
public class UnityPlayerActivity extends Activity
{
	protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code
	// Setup activity layout
	@Override protected void onCreate (Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		getWindow().setFormat(PixelFormat.RGBX_8888); // <--- This makes xperia play happy
		if (mUnityPlayer != null) {
			mUnityPlayer.quit();
			mUnityPlayer = null;
		}
		mUnityPlayer = new UnityPlayer(this);
		setContentView(mUnityPlayer);
		mUnityPlayer.requestFocus();
		Log.w("Unity", "Unity Access Here");

	}
	/**
	 * Workaround for when a message is sent containing both a Data and Notification payload.
	 *
	 * When the app is in the background, if a message with both a data and notification payload is
	 * receieved the data payload is stored on the Intent passed to onNewIntent. By default, that
	 * intent does not get set as the Intent that started the app, so when the app comes back online
	 * it doesn't see a new FCM message to respond to. As a workaround, we override onNewIntent so
	 * that it sends the intent to the MessageForwardingService which forwards the message to the
	 * FirebaseMessagingService which in turn sends the message to the application.
	 */
	@Override
	protected void onNewIntent(Intent intent) {
		Intent message = new Intent(this, MessageForwardingService.class);
		message.setAction(MessageForwardingService.ACTION_REMOTE_INTENT);
		message.putExtras(intent);
		message.setData(intent.getData());
		startService(message);
	}

	public void UnityTest() {
		Log.w("Unity", "Test Native");
	}
	public String UnityGetCountryCode(){
		Log.w("Unity", "GetCountryCode Native");
		String country="";
		try
		{
			TelephonyManager tm = (TelephonyManager)getSystemService(getApplicationContext().TELEPHONY_SERVICE);
			country = tm.getNetworkCountryIso();

			Log.w("Unity", "GetCountryCode1:"+country+","+country);
		}
		catch(Exception ex){
			Log.w("Unity", "GetCountryCode Telephone Exception:"+ex.getMessage());
		}
		if(country.length()<1){
			try
			{
				Log.w("Unity", "GetCountryCode Native");
				country = mUnityPlayer.getContext().getResources().getConfiguration().locale.getCountry();
				String countryName = mUnityPlayer.getContext().getResources().getConfiguration().locale.getDisplayCountry();

				Log.w("Unity", "GetCountryCode:"+country+","+countryName);
				return country+","+countryName;
			}
			catch(Exception ex){
				Log.w("Unity", "GetCountryCode Exception:"+ex.getMessage());
				return "";
			}
		}
		else{
			return country+","+country;
		}
	}

	private void InitSP(boolean noChange){
		Log.w("Unity", "InitSP");
		// Get the device's sample rate and buffer size to enable low-latency Android audio output, if available.
		String samplerateString = null, buffersizeString = null;
		if (Build.VERSION.SDK_INT >= 17) {
			AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
			samplerateString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
			buffersizeString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);
		}
		if (samplerateString == null) samplerateString = "44100";
		if (buffersizeString == null) buffersizeString = "512";
		// Arguments: path to the APK file, offset and length of the two resource files, sample rate, audio buffer size.
		SuperpoweredExample(Integer.parseInt(samplerateString), Integer.parseInt(buffersizeString));
		onSetChangePitch(noChange);
		Log.w("Unity", "InitSP Finish");

	}
	private void settingMusicPlayer(String filePath,int offset,int fileLength){
		onSettingMusicPlayer(filePath, offset, fileLength);
	}

	// Quit Unity
	@Override protected void onDestroy ()
	{
		mUnityPlayer.quit();
		super.onDestroy();
	}

	// Pause Unity
	@Override protected void onPause()
	{
		super.onPause();
		mUnityPlayer.pause();
	}

	// Resume Unity
	@Override protected void onResume()
	{
		super.onResume();
		mUnityPlayer.resume();
		try {
			NotificationManager manager = (NotificationManager) getApplicationContext()
					.getSystemService(Context.NOTIFICATION_SERVICE);
			manager.cancelAll();
		}
		catch(Exception e){

		}
	}

	// This ensures the layout will be correct.
	@Override public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		mUnityPlayer.configurationChanged(newConfig);
	}

	// Notify Unity of the focus change.
	@Override public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);
		mUnityPlayer.windowFocusChanged(hasFocus);
	}

	// For some reason the multiple keyevent type is not supported by the ndk.
	// Force event injection by overriding dispatchKeyEvent().
	@Override public boolean dispatchKeyEvent(KeyEvent event)
	{
		if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
			return mUnityPlayer.injectEvent(event);
		return super.dispatchKeyEvent(event);
	}

	// Pass any events not handled by (unfocused) views straight to UnityPlayer
	@Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
	@Override public boolean onKeyDown(int keyCode, KeyEvent event)   { return mUnityPlayer.injectEvent(event); }
	@Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
	/*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }


	private native void SuperpoweredExample(int samplerate, int buffersize);
	private native void onSettingMusicPlayer(String filePath, int offset,int length);
	private native void onPlayPause(boolean play);
	private native void onChangePitch(float pitch);
	private native void onChangeVolume(float volume);
	private native void onPlayAtPosition(long positionms,float volume,float pitch);
	private native boolean onIsPlay();
	private native void onReleaseData();
	private native int onClipLength();
	private native float onCurrentTimeInClip();
	private native void onChangeVolumeAndPitch(float volume,float pitch);
	private native void onSeek(float seek);
	private native void onCrossfader(int value);
	private native void onFxSelect(int value);
	private native void onFxOff();
	private native void onFxValue(int value);
	private native float onGetCurrentVolume();
	private native void onSetChangePitch(boolean onNoChange);

	static {
		System.loadLibrary("SuperpoweredExample");
	}
	/*
	float volume=0;
	public void Init(){
		volume+=0.1f;
		if(volume<0)
			volume=1;
		Log.e("Unity", "Unity Init Ne Java");
		//onPlayPause(volume);
		onPlayAtPosition(4000,1,1);
	}
	float pitch=1;
	public void ChangePitch(){
		pitch+=0.1f;
		if(pitch>3)
			pitch=0.5f;
		Log.e("Unity", "Unity ChangePitch Ne Java");
		onChangePitch(pitch);

	}*/

	public boolean UnityLoadMusicPlayer(String filePath,boolean isLocal,String songName){
		try{
			String path = "";
			int fileAoffset = 0;
			int fileAlength = 0;
			if(isLocal){
				AssetFileDescriptor fd0 = getAssets().openFd(songName);
				fileAoffset = (int)fd0.getStartOffset();
				fileAlength = (int)fd0.getLength();
				path = getPackageResourcePath();
				try {

					fd0.getParcelFileDescriptor().close();

				} catch (IOException e) {

					Log.e("Unity", "Close error");
				}
			}else {

				File file = new File(filePath);
				fileAlength = (int) file.length();
				path = filePath;
			}

			//File file = new File(filePath);
			onSettingMusicPlayer(path, fileAoffset, fileAlength);
			//onSettingMusicPlayer(filePath, 0, (int) file.length());


			//Log.e("Unity", "Load SP Finish:" + getPackageResourcePath()+","+fileAoffset);
			return true;
		}
		catch (Exception ex){
			Log.e("UnityError","UnityLoadMusicPlayer"+ex.getMessage());
			return false;
		}
	}

	public void UnityReleaseOldData(){
		onReleaseData();
	}
	public float UnityGetClipLength(){
		int temp= onClipLength();
		//Log.e("UNITY","Unity UnityGetClipLength:"+temp);
		return (float)temp/1000.0f;
	}
	public float UnityGetCurrentVolume(){
		float temp= onGetCurrentVolume();
		Log.e("UNITY","Unity UnityGetClipLength:"+temp);
		return temp;
	}
	public float UnityCurrentTimeInClip(){
		float temp= onCurrentTimeInClip();
		//Log.e("UNITY","UnityCurrentTimeInClip:"+temp);
		return temp/1000.0f;
	}
	public boolean UnityIsPlay(){
		return onIsPlay();
	}
	public void UnityPlayOrPause(boolean isPause){
		//Log.e("UNITY","Unity onPlayPause:"+isPause);
		onPlayPause(isPause);
	}
	public void UnityPlayMuteAtStart(){
		//Log.e("UNITY","Unity UnityPlayMuteAtStart");
		onPlayAtPosition(0, 0, 1);
	}
	public void UnityPlayAtPostion(long timeMS,float volume,float pitch){
		//Log.e("UNITY","Unity UnityPlayAtPostion:"+timeMS+","+volume+","+pitch);...
		onPlayAtPosition(timeMS, volume, pitch);

	}
	public void UnityPlayChangePitchAndVolume(float volume,float pitch){
		//Log.e("UNITY","Unity onChangeVolumeAndPitch:"+volume+","+pitch);
		onChangeVolumeAndPitch(volume, pitch);
	}
	public void UnityChangePitch(float pitch){
		//Log.e("UNITY","Unity onChangePitch:"+pitch);
		onChangePitch(pitch);
	}
	public void UnityChangeVolume(float volume){
		//Log.e("UNITY","Unity UnityChangeVolume:"+volume);
		onChangeVolume(volume);
	}
	public void UnitySetNewGame(boolean noChangePitch){
		//noChange = noChangePitch;
		InitSP(noChangePitch);
	}


	/*public void OnCallFilePath(String filePath){
		Log.e("Unity", "Unity OnCallFilePath:"+filePath);
		try
		{
			String str="file://"+filePath;
			Log.e("Unity call A",str);

			//FileDescriptor descriptor = getAssets().openFd(str);

			File file = new File(filePath);
			Log.e("Unity001","PathFile:"+file.getAbsoluteFile()+"");
			Log.e("Unity001","FileLength:"+file.length()+"");
			FileInputStream fos1 = new FileInputStream(file);

			FileDescriptor fd = fos1.getFD();
			//passing FileDescriptor to another  FileOutputStream
			onSettingMusicPlayer(filePath,0,(int)file.length());
			Log.e("Unity call A","OKKK"+fd.toString());
			fos1.close();
			onPlayAtPosition(0,0.0f,1);
		}
		catch (Exception ex){
			Log.e("UnityError",ex.getMessage());
		}
	}*/
	public static void TestStatic(){
		Log.e("Unity", "Unity TestStatic Ne Java");
	}

	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if(Build.VERSION.SDK_INT < 23) {
			return;
		}
		switch (requestCode) {
			case 0:
				if (grantResults.length > 0) {
					boolean isChecked = false;
					for (int i =0;i<grantResults.length;i++ ){
						if(grantResults[i] == PackageManager.PERMISSION_GRANTED){
							UnityPlayer.UnitySendMessage("UniAndroidPermission", "RequestPermissionResult", "USER_REPOSNE");
							isChecked = true;
							break;
						}
					}
					if (!isChecked){
						for (int i = 0; i< PermissionManager.mPermissionStr.length; i++){
							if(PermissionManager.perms.get(PermissionManager.mPermissionStr[i])){
								isChecked = true;
								break;
							}
						}
						if(isChecked){
							UnityPlayer.UnitySendMessage("UniAndroidPermission", "RequestPermissionResult", "USER_REPOSNE");
						}else {
							Boolean isNerverAsk = true;
							for (int i =0;i<PermissionManager.mPermissionStr.length;i++){

								if(UnityPlayer.currentActivity.shouldShowRequestPermissionRationale(PermissionManager.mPermissionStr[i])){
									isNerverAsk = false;
									break;
								}
							}
							if(isNerverAsk){
								UnityPlayer.UnitySendMessage("UniAndroidPermission", "RequestPermissionResult", "NERVER_ASK_ACTIVED");
							}else {
								UnityPlayer.UnitySendMessage("UniAndroidPermission", "RequestPermissionResult", "USER_REPOSNE");
							}
						}
					}
					//UnityPlayer.UnitySendMessage("UniAndroidPermission", "RequestPermissionResult", "USER_REPOSNE_DENY");
				}
				else
				{
					// if(UnityPlayer.currentActivity.shouldShowRequestPermissionRationale(mPermissionStr))
					// {
					UnityPlayer.UnitySendMessage("UniAndroidPermission", "RequestPermissionResult", "NERVER_ASK_ACTIVED");
				}
				break;
			default:
				UnityPlayer.UnitySendMessage("UniAndroidPermission", "RequestPermissionResult", "OTHER");
				break;

		}
	}

//	public boolean CheckEnableNotification(){
//		return Settings.Secure.getString(this.getContentResolver(),
//				"enabled_notification_listeners")
//				.contains(getApplicationContext()
//				.getPackageName());
//	}

}