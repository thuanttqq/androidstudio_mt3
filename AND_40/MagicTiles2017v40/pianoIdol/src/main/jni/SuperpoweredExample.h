#ifndef Header_SuperpoweredExample
#define Header_SuperpoweredExample

#include <math.h>
#include <pthread.h>

#include "SuperpoweredExample.h"
#include <SuperpoweredAdvancedAudioPlayer.h>
#include <SuperpoweredFilter.h>
#include <SuperpoweredRoll.h>
#include <SuperpoweredFlanger.h>
#include <AndroidIO/SuperpoweredAndroidAudioIO.h>

#define HEADROOM_DECIBEL 3.0f
static const float headroom = powf(10.0f, -HEADROOM_DECIBEL * 0.025f);

class SuperpoweredExample {
public:

	SuperpoweredExample(unsigned int samplerate, unsigned int buffersize);

	~SuperpoweredExample();
	void settingMusicPlayer(const char *path, int fileAoffset, int fileAlength);
	bool process(short int *output, unsigned int numberOfSamples);
	void onPlayPause(bool play);
	void onCrossfader(int value);
	void onFxSelect(int value);
	void onFxOff();
	void onFxValue(int value);
	void onChangeVolume(float value);
	void onPlayAtPosition(long position,float volume,float pitch);
	void onSeek(float value);
	void onChangePitch(float value);
	bool onIsPlay();
	int onClipLength();
	float onCurrentTimeInClip();
	float onGetCurrentVolume();
	void onReleaseData();
	void onChangeVolumeAndPitch(float volume,float tempo);
	void onSetChangePitch(bool noChangePitch);
	float fast=1;
	bool noChangePitchInGame = true;
private:
    pthread_mutex_t mutex;
    SuperpoweredAndroidAudioIO *audioSystem;
    SuperpoweredAdvancedAudioPlayer *playerA;
	float *stereoBuffer;
	float volA=1;
	float volumeA=1;
	unsigned int bufferCache=0;
	unsigned int sampleRate=0;
	/* SuperpoweredRoll *roll;
    SuperpoweredFilter *filter;
    SuperpoweredFlanger *flanger;
    float *stereoBuffer;
    unsigned char activeFx;
    float crossValue, volA;*/
};

#endif
