#include "SuperpoweredExample.h"
#include <SuperpoweredSimple.h>
#include <jni.h>
#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include <android/log.h>
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_AndroidConfiguration.h>

static void playerEventCallbackA(void *clientData, SuperpoweredAdvancedAudioPlayerEvent event, void * __unused value) {
    if (event == SuperpoweredAdvancedAudioPlayerEvent_LoadSuccess) {
    	SuperpoweredAdvancedAudioPlayer *playerA = *((SuperpoweredAdvancedAudioPlayer **)clientData);
        playerA->setBpm(126.0f);
        playerA->setFirstBeatMs(353);
        playerA->setPosition(playerA->firstBeatMs, false, false);
    };
}

static bool audioProcessing(void *clientdata, short int *audioIO, int numberOfSamples, int __unused samplerate) {
	return ((SuperpoweredExample *)clientdata)->process(audioIO, (unsigned int)numberOfSamples);
}

SuperpoweredExample::SuperpoweredExample(unsigned int samplerate, unsigned int buffersize) :  volA(1.0f * headroom) {
    bufferCache=buffersize;
    sampleRate=samplerate;
    stereoBuffer = (float *)memalign(16, (bufferCache + 16) * sizeof(float) * 2);
    playerA = new SuperpoweredAdvancedAudioPlayer(&playerA , playerEventCallbackA, samplerate, 0);
    audioSystem = new SuperpoweredAndroidAudioIO(samplerate, buffersize, false, true, audioProcessing, this, -1, SL_ANDROID_STREAM_MEDIA, buffersize * 2);

}


SuperpoweredExample::~SuperpoweredExample() {
    delete audioSystem;
    delete playerA;
    free(stereoBuffer);
}
void SuperpoweredExample::onSetChangePitch(bool _noChange) {
    noChangePitchInGame = _noChange;
}

void SuperpoweredExample::settingMusicPlayer(const char *path, int fileAoffset, int fileAlength){
    if(playerA!=nullptr)
        delete playerA;
    if(stereoBuffer!= nullptr)
        free(stereoBuffer);
    stereoBuffer = (float *)memalign(16, (bufferCache + 16) * sizeof(float) * 2);
    playerA = new SuperpoweredAdvancedAudioPlayer(&playerA , playerEventCallbackA, sampleRate, 0);

    playerA->open(path, fileAoffset, fileAlength);
    playerA->syncMode = SuperpoweredAdvancedAudioPlayerSyncMode_TempoAndBeat;
}
void SuperpoweredExample::onPlayPause(bool play) {
    if (!play) {
        playerA->pause();
        playerA->setTempo(fast, noChangePitchInGame);
    } else {
        playerA->setTempo(fast, noChangePitchInGame);
        playerA->play(true);
        //playerA -> looping = false;
    };
}
void SuperpoweredExample::onChangePitch(float value=1) {
    if(value>3){
        value=3;
    }
    else if(value<0){
        value=0.1f;
    }
    playerA->setTempo(value, noChangePitchInGame);
    //playerA -> looping = false;
}

void SuperpoweredExample::onSeek(float value=1) {
    if(value<0){
        value=0;
    }
    playerA->seek(value);
}
void SuperpoweredExample::onPlayAtPosition(long position,float volume,float pitch){

    playerA->setPosition(position,false,false);
    volA=volume * headroom;
    volumeA=volume;
    playerA->setTempo(pitch, noChangePitchInGame);

    if(!playerA->playing){
        fast = pitch;
        //playerA->looping= false;
        playerA->play(true);
        //playerA->looping = false;
    }
}

bool SuperpoweredExample::onIsPlay() {
    return  playerA->playing;
}

int SuperpoweredExample::onClipLength() {
  //  __android_log_print(ANDROID_LOG_ERROR,"Unity","Clip Length:%d",playerA->durationMs);
   return (int)playerA->durationMs;
}

float SuperpoweredExample::onCurrentTimeInClip() {
    return (float)playerA->positionMs;
}
float SuperpoweredExample::onGetCurrentVolume() {
    return volumeA;
}


void SuperpoweredExample::onReleaseData() {
}

void SuperpoweredExample::onChangeVolumeAndPitch(float volume,float tempo) {
    volumeA=volume;
    volA=volume * headroom;
    playerA->setTempo(tempo, noChangePitchInGame);
}

void SuperpoweredExample::onChangeVolume(float value=1) {
    volumeA=value;
    volA=value * headroom;
}

void SuperpoweredExample::onCrossfader(int value) {

}

void SuperpoweredExample::onFxSelect(int value) {
	__android_log_print(ANDROID_LOG_VERBOSE, "SuperpoweredExample", "FXSEL %i", value);
}

void SuperpoweredExample::onFxOff() {
}

#define MINFREQ 60.0f
#define MAXFREQ 20000.0f

static inline float floatToFrequency(float value) {
    if (value > 0.97f) return MAXFREQ;
    if (value < 0.03f) return MINFREQ;
    value = powf(10.0f, (value + ((0.4f - fabsf(value - 0.4f)) * 0.3f)) * log10f(MAXFREQ - MINFREQ)) + MINFREQ;
    return value < MAXFREQ ? value : MAXFREQ;
}

void SuperpoweredExample::onFxValue(int ivalue) {

}

bool SuperpoweredExample::process(short int *output, unsigned int numberOfSamples) {
    double masterBpm =  playerA->currentBpm ;//masterIsA ? playerA->currentBpm : playerB->currentBpm;
    double msElapsedSinceLastBeatA = playerA->msElapsedSinceLastBeat;
    bool silence = !playerA->process(stereoBuffer, false, numberOfSamples, volA, masterBpm,playerA->msElapsedSinceLastBeat);

    // The stereoBuffer is ready now, let's put the finished audio into the requested buffers.
    if (!silence) SuperpoweredFloatToShortInt(stereoBuffer, output, numberOfSamples);
    return !silence;
}

static SuperpoweredExample *example = NULL;

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_SuperpoweredExample(JNIEnv *javaEnvironment, jobject __unused obj, jint samplerate, jint buffersize) {
    example = new SuperpoweredExample((unsigned int)samplerate, (unsigned int)buffersize);
}

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onSettingMusicPlayer(JNIEnv *javaEnvironment, jobject __unused obj, jstring filePath, jint fileAoffset, jint fileAlength) {
    const char *path = javaEnvironment->GetStringUTFChars(filePath, JNI_FALSE);
    example->settingMusicPlayer(path, fileAoffset, fileAlength);
    javaEnvironment->ReleaseStringUTFChars(filePath, path);
}

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onPlayPause(JNIEnv * __unused javaEnvironment, jobject __unused obj, jboolean play) {
	example->onPlayPause(play);
}

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onSetChangePitch(JNIEnv * __unused javaEnvironment, jobject __unused obj, jboolean _onNoChange) {
    example->onSetChangePitch(_onNoChange);
}
extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onChangePitch(JNIEnv * __unused javaEnvironment, jobject __unused obj, jfloat pitch) {
    example->onChangePitch(pitch);
}
extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onSeek(JNIEnv * __unused javaEnvironment, jobject __unused obj, jfloat seek) {
    example->onSeek(seek);
}

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onPlayAtPosition(JNIEnv * __unused javaEnvironment, jobject __unused obj, jlong location,jfloat volume,jfloat pitch) {
    example->onPlayAtPosition(location,volume,pitch);
}

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onChangeVolume(JNIEnv * __unused javaEnvironment, jobject __unused obj, jfloat volume) {
    example->onChangeVolume(volume);
}

extern "C" JNIEXPORT bool Java_com_youmusic_magictiles_UnityPlayerActivity_onIsPlay(JNIEnv * __unused javaEnvironment, jobject __unused obj) {
    return example->onIsPlay();
}

extern "C" JNIEXPORT int Java_com_youmusic_magictiles_UnityPlayerActivity_onClipLength(JNIEnv * __unused javaEnvironment, jobject __unused obj) {
    int clipLenth=example->onClipLength();
    return clipLenth;
}

extern "C" JNIEXPORT float Java_com_youmusic_magictiles_UnityPlayerActivity_onCurrentTimeInClip(JNIEnv * __unused javaEnvironment, jobject __unused obj) {

    float clipTime=example->onCurrentTimeInClip();
    //  __android_log_print(ANDROID_LOG_ERROR,"Unity","Clip Time:%f",clipTime);

    return clipTime;
}

extern "C" JNIEXPORT float Java_com_youmusic_magictiles_UnityPlayerActivity_onGetCurrentVolume(JNIEnv * __unused javaEnvironment, jobject __unused obj) {
    float volume=example->onGetCurrentVolume();
      __android_log_print(ANDROID_LOG_ERROR,"Unity","Volume:%f",volume);

    return volume;
}

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onReleaseData(JNIEnv * __unused javaEnvironment, jobject __unused obj) {
    example->onReleaseData();
}
extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onChangeVolumeAndPitch(JNIEnv * __unused javaEnvironment, jobject __unused obj,jfloat volume,jfloat pitch) {
    example->onChangeVolumeAndPitch(volume,pitch);
}


extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onCrossfader(JNIEnv * __unused javaEnvironment, jobject __unused obj, jint value) {
	example->onCrossfader(value);
}

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onFxSelect(JNIEnv * __unused javaEnvironment, jobject __unused obj, jint value) {
	example->onFxSelect(value);
}

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onFxOff(JNIEnv * __unused javaEnvironment, jobject __unused obj) {
	example->onFxOff();
}

extern "C" JNIEXPORT void Java_com_youmusic_magictiles_UnityPlayerActivity_onFxValue(JNIEnv * __unused javaEnvironment, jobject __unused obj, jint value) {
	example->onFxValue(value);
}
