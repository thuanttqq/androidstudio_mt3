package com.youmusic.magictiles;

/**
 * Created by Luffy on 11/14/17.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

/**
 * To test a referrer broadcast from ADB:
 *
 * adb shell
 *
 * am broadcast -a com.android.vending.INSTALL_REFERRER -n com.example.android.custom.referrer.receiver/.ReferrerReceiver --es "referrer" "utm_source=YourAppName&utm_medium=YourMedium&utm_campaign=YourCampaign&utm_content=YourSampleContent"
 */
public class ReferrerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        new com.appsflyer.MultipleInstallBroadcastReceiver().onReceive(context, intent);
        //new com.tapjoy.InstallReferrerReceiver().onReceive(context, intent);


        Log.e("Unity", "UNITY ReferrerReceiver:onReceive");

        final String action = intent.getAction();
        String referSource="None";
        String campaign="None";
        if (action != null && TextUtils.equals(action, "com.android.vending.INSTALL_REFERRER")) {
            Log.e("Unity", "UNITY ReferrerReceiver:onReceive->com.android.vending.INSTALL_REFERRER");

            try {
                final String referrer = intent.getStringExtra("referrer");
                Log.e("Unity", "UNITY ReferrerReceiver:onReceive referrer:"+referrer);

                if(referrer.contains("pid")){ //appflyer
                    String[] params =null;
                    if(referrer.contains("&")){
                        params=referrer.split("&");
                    }
                    if(params==null){
                        params=referrer.split("%26");// ASCII;
                    }
                    for (String p : params) {
                        if(p.contains("pid")){
                            String pid=p.replace("pid=","").replace("pid%3D","");
                            referSource="Appsflyer "+pid;
                        }
                        else if(p.contains("c=")){
                            String cam=p.replace("c=","");
                            campaign=cam;
                        }
                        else if(p.contains("c%3D")){
                            String cam=p.replace("c%3D","");
                            campaign=cam;
                        }
                    }
                }
                else if(referrer.contains("ts")){// tapstream
                    String cam=referrer.replace("ts=","").replace("ts%3D","");
                    referSource="tapstream";
                    campaign=cam;
                }
                else if(referrer.length()>1){//other
                    referSource="Other";
                }
                else{// organic
                    referSource="Other";
                }

                Log.e("Unity", "UNITY ReferrerReceiver:onReceive referSource:"+referSource+","+campaign);
                UnityPlayer.UnitySendMessage("UniAndroidPermission", "ReferrerSourceTracking", referSource);
                UnityPlayer.UnitySendMessage("UniAndroidPermission", "ReferrerCampaignTracking", campaign);

            } catch (Exception e) {
                e.printStackTrace();
            }

            /**
             * OPTIONAL: Forward the intent to Google Analytics V2 receiver
             */
            // new com.google.analytics.tracking.android.AnalyticsReceiver().onReceive(context, intent);
        }

    }
}