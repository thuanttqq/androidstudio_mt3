#/bin/bash
export PWD=$(pwd)

cd /Users/sever-dev/Documents/GitRepo/pianoidol2
git pull
cd $PWD

echo Run Unity to export Android Studio project
rm -rf "/Users/sever-dev/Desktop/PI_unityExport/android"
"/Applications/Unity 5.4.2/Unity.app/Contents/MacOS/Unity" -batchmode -nographics -projectpath /Users/sever-dev/Documents/GitRepo/pianoidol2 -executeMethod CIEditorScript.PerformAndroidBuild -quit
rm -rf /Users/sever-dev/Desktop/AndroidProject/MagicTiles2017/pianoIdol/src/main/assets
cp -R "/Users/sever-dev/Desktop/PI_unityExport/android/Instrument Expert/assets" /Users/sever-dev/Desktop/AndroidProject/MagicTiles2017/pianoIdol/src/main

echo Build the Android APK
./gradlew assembleRelease

echo Copy APK to release folder
rm /Users/sever-dev/Desktop/PI_Build/pianoIdol-release.apk
cp ./pianoIdol/build/outputs/apk/pianoIdol-release.apk /Users/sever-dev/Desktop/PI_Build
